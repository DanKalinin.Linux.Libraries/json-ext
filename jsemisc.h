//
// Created by root on 13.09.2021.
//

#ifndef LIBRARY_JSON_EXT_JSEMISC_H
#define LIBRARY_JSON_EXT_JSEMISC_H

#include "jsemain.h"

G_BEGIN_DECLS

#define JSE_MIME_TYPE "application/json"

G_END_DECLS

#endif //LIBRARY_JSON_EXT_JSEMISC_H
