//
// Created by root on 19.10.2021.
//

#include "jseinit.h"

void jse_init(void) {
    jse_json_init();
}

void jse_init_i18n(gchar *directory) {
    jse_json_init_i18n(directory);
}

void jse_init_po(void) {
    jse_json_init_po();
}
