//
// Created by Dan on 10.03.2025.
//

#ifndef LIBRARY_JSON_EXT_JSEJSONINIT_H
#define LIBRARY_JSON_EXT_JSEJSONINIT_H

#include "jsemain.h"
#include "jsejsonlcmessages.h"

G_BEGIN_DECLS

void jse_json_init(void);
void jse_json_init_i18n(gchar *directory);
void jse_json_init_po(void);

G_END_DECLS

#endif //LIBRARY_JSON_EXT_JSEJSONINIT_H
