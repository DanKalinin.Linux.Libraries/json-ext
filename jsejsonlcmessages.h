//
// Created by Dan on 10.03.2025.
//

#ifndef LIBRARY_JSON_EXT_JSEJSONLCMESSAGES_H
#define LIBRARY_JSON_EXT_JSEJSONLCMESSAGES_H

#include "jsemain.h"

G_BEGIN_DECLS

extern gchar jse_json_lc_messages_ru_data[];
extern gint jse_json_lc_messages_ru_n;

gboolean jse_json_lc_messages_set(gchar *file, GError **error);

G_END_DECLS

#endif //LIBRARY_JSON_EXT_JSEJSONLCMESSAGES_H
