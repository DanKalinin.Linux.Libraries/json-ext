//
// Created by Dan on 10.03.2025.
//

#include "jsejsoninit.h"
#include "jsejsoni18n.h"

void jse_json_init(void) {

}

void jse_json_init_i18n(gchar *directory) {
    (void)bindtextdomain(GETTEXT_PACKAGE, directory);
    (void)bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
}

void jse_json_init_po(void) {
    gchar *directory = bindtextdomain(GETTEXT_PACKAGE, NULL);
    gchar *language = (gchar *)g_getenv("LANG");
    g_autofree gchar *file = g_build_filename(directory, language, "LC_MESSAGES", GETTEXT_PACKAGE ".mo", NULL);
    if (!jse_json_lc_messages_set(file, NULL)) return;
}
