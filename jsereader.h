//
// Created by dan on 23.08.2019.
//

#ifndef LIBRARY_JSON_EXT_JSEREADER_H
#define LIBRARY_JSON_EXT_JSEREADER_H

#include "jsemain.h"










G_BEGIN_DECLS

gboolean jse_reader_read_element(JsonReader *self, guint index, GError **error);
gint jse_reader_count_elements(JsonReader *self, GError **error);

gboolean jse_reader_read_member(JsonReader *self, gchar *name, GError **error);

G_END_DECLS










G_BEGIN_DECLS

#define JSE_TYPE_READER jse_reader_get_type()

GE_DECLARE_DERIVABLE_TYPE(JSEReader, jse_reader, JSE, READER, JsonReader)

struct _JSEReaderClass {
    JsonReaderClass super;
};

struct _JSEReader {
    JsonReader super;
};

G_END_DECLS










#endif //LIBRARY_JSON_EXT_JSEREADER_H
