//
// Created by dan on 23.08.2019.
//

#ifndef LIBRARY_JSON_EXT_JSEGENERATOR_H
#define LIBRARY_JSON_EXT_JSEGENERATOR_H

#include "jsemain.h"

G_BEGIN_DECLS

#define JSE_TYPE_GENERATOR jse_generator_get_type()

GE_DECLARE_DERIVABLE_TYPE(JSEGenerator, jse_generator, JSE, GENERATOR, JsonGenerator)

struct _JSEGeneratorClass {
    JsonGeneratorClass super;
};

struct _JSEGenerator {
    JsonGenerator super;
};

G_END_DECLS

#endif //LIBRARY_JSON_EXT_JSEGENERATOR_H
