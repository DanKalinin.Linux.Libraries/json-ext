//
// Created by Dan on 10.03.2025.
//

#include "jsejsonlcmessages.h"

gboolean jse_json_lc_messages_set(gchar *file, GError **error) {
    gchar *language = (gchar *)g_getenv("LANG");
    if (g_str_equal(language, "ru")) return ge_file_set_contents_mkdir(file, jse_json_lc_messages_ru_data, jse_json_lc_messages_ru_n, error);
    return TRUE;
}
