//
// Created by dan on 23.08.2019.
//

#ifndef LIBRARY_JSON_EXT_JSEPARSER_H
#define LIBRARY_JSON_EXT_JSEPARSER_H

#include "jsemain.h"

G_BEGIN_DECLS

#define JSE_TYPE_PARSER jse_parser_get_type()

GE_DECLARE_DERIVABLE_TYPE(JSEParser, jse_parser, JSE, PARSER, JsonParser)

struct _JSEParserClass {
    JsonParserClass super;
};

struct _JSEParser {
    JsonParser super;
};

G_END_DECLS

#endif //LIBRARY_JSON_EXT_JSEPARSER_H
