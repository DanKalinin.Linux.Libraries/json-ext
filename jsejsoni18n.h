//
// Created by Dan on 10.03.2025.
//

#ifndef LIBRARY_JSON_EXT_JSEJSONI18N_H
#define LIBRARY_JSON_EXT_JSEJSONI18N_H

#include "jsemain.h"

G_BEGIN_DECLS

#define GETTEXT_PACKAGE "json-glib-1.0"
#include <glib/gi18n-lib.h>

G_END_DECLS

#endif //LIBRARY_JSON_EXT_JSEJSONI18N_H
