//
// Created by dan on 23.08.2019.
//

#include "jsereader.h"










gboolean jse_reader_read_element(JsonReader *self, guint index, GError **error) {
    gboolean ret = json_reader_read_element(self, index);

    if (!ret) {
        GE_SET_VALUE(error, g_error_copy(json_reader_get_error(self)));
    }

    return ret;
}

gint jse_reader_count_elements(JsonReader *self, GError **error) {
    gint ret = json_reader_count_elements(self);

    if (ret == -1) {
        GE_SET_VALUE(error, g_error_copy(json_reader_get_error(self)));
    }

    return ret;
}

gboolean jse_reader_read_member(JsonReader *self, gchar *name, GError **error) {
    gboolean ret = json_reader_read_member(self, name);

    if (!ret) {
        GE_SET_VALUE(error, g_error_copy(json_reader_get_error(self)));
    }

    return ret;
}










G_DEFINE_TYPE(JSEReader, jse_reader, JSON_TYPE_READER)

static void jse_reader_class_init(JSEReaderClass *class) {

}

static void jse_reader_init(JSEReader *self) {

}
