//
// Created by root on 19.10.2021.
//

#ifndef LIBRARY_JSON_EXT_JSEINIT_H
#define LIBRARY_JSON_EXT_JSEINIT_H

#include "jsemain.h"
#include "jsejsoninit.h"

G_BEGIN_DECLS

void jse_init(void);
void jse_init_i18n(gchar *directory);
void jse_init_po(void);

G_END_DECLS

#endif //LIBRARY_JSON_EXT_JSEINIT_H
