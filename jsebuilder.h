//
// Created by dan on 23.08.2019.
//

#ifndef LIBRARY_JSON_EXT_JSEBUILDER_H
#define LIBRARY_JSON_EXT_JSEBUILDER_H

#include "jsemain.h"

G_BEGIN_DECLS

#define JSE_TYPE_BUILDER jse_builder_get_type()

GE_DECLARE_DERIVABLE_TYPE(JSEBuilder, jse_builder, JSE, BUILDER, JsonBuilder)

struct _JSEBuilderClass {
    JsonBuilderClass super;
};

struct _JSEBuilder {
    JsonBuilder super;
};

G_END_DECLS

#endif //LIBRARY_JSON_EXT_JSEBUILDER_H
