//
// Created by Dan on 10.03.2025.
//

#include "jsejsoni18n.h"

G_DEFINE_CONSTRUCTOR(jse_json_i18n_constructor)

static void jse_json_i18n_constructor(void) {
    (void)bindtextdomain(GETTEXT_PACKAGE, NULL);
    (void)bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
}
